/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "TR_ROLE_MAPPING", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "TrRoleMapping.findAll", query = "SELECT t FROM TrRoleMapping t"),
		@NamedQuery(name = "TrRoleMapping.findById", query = "SELECT t FROM TrRoleMapping t WHERE t.id = :id"),
		@NamedQuery(name = "TrRoleMapping.findByIsDeleted", query = "SELECT t FROM TrRoleMapping t WHERE t.isDeleted = :isDeleted"),
		@NamedQuery(name = "TrRoleMapping.findByCreatedDate", query = "SELECT t FROM TrRoleMapping t WHERE t.createdDate = :createdDate") })
public class TrRoleMapping implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false)
	private Long id;
	@Column(name = "IS_DELETED")
	private Boolean isDeleted;
	@Basic(optional = false)
	@Column(name = "CREATED_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private MsRole roleId;
	@JoinColumn(name = "CLIENT_DETAIL_ID", referencedColumnName = "CLIENT_DETAIL_ID", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private TrClientDetail clientDetailId;

	public TrRoleMapping() {
	}

	public TrRoleMapping(Long id) {
		this.id = id;
	}

	public TrRoleMapping(Long id, Date createdDate) {
		this.id = id;
		this.createdDate = createdDate;
	}

	public TrRoleMapping(MsRole roleId, TrClientDetail clientDetailId, Boolean isDeleted, Date createdDate) {
		super();
		this.isDeleted = isDeleted;
		this.createdDate = createdDate;
		this.roleId = roleId;
		this.clientDetailId = clientDetailId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public MsRole getRoleId() {
		return roleId;
	}

	public void setRoleId(MsRole roleId) {
		this.roleId = roleId;
	}

	public TrClientDetail getClientDetailId() {
		return clientDetailId;
	}

	public void setClientDetailId(TrClientDetail clientDetailId) {
		this.clientDetailId = clientDetailId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof TrRoleMapping)) {
			return false;
		}
		TrRoleMapping other = (TrRoleMapping) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.mtl.api.entity.TrRoleMapping[ id=" + id + " ]";
	}

}
