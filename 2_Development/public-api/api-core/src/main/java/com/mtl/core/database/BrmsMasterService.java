package com.mtl.core.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.mtl.model.master.DecisionTableModel;

import lombok.Data;

@Data
@Service
public class BrmsMasterService {

	private DataSource ds;
	private EntityManager em;
	private String sql = "SELECT * FROM API.TR_ROLE_MAPPING ";

	public BrmsMasterService(EntityManager em, DataSource ds) {
		super();
		this.em = em;
		this.ds = ds;
		load();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public void load() {
		
		DecisionTableModel dt = new DecisionTableModel();
		
		try (Connection conn = ds.getConnection();
				Statement stmt = conn.createStatement();

				ResultSet rs = stmt.executeQuery(sql);) {

			ResultSetMetaData resultSetMetaData = rs.getMetaData();
			for(int i = 0; i< resultSetMetaData.getColumnCount(); i++ ) {
				dt.putColumn(i, resultSetMetaData.getColumnName(1));
//				resultSetMetaData.
			}

			System.out.println("1. Column count in users table :: " + resultSetMetaData.getColumnCount());
			System.out.println("2. First column name in users table :: " + resultSetMetaData.getColumnName(1));
			System.out.println("3. Database name of users table' column id :: " + resultSetMetaData.getCatalogName(1));

			System.out.println("4. Data type of column id :: " + resultSetMetaData.getColumnTypeName(1));
			System.out.println("5. Get table name of column id :: " + resultSetMetaData.getTableName(1));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		List<Object> result = em.createNativeQuery(sql).getResultList();

		System.out.println(result.size());
		for (Object row : result) {
			Object[] columns = (Object[]) row;
			for (Object col : columns) {
				System.out.print(col + " ");
			}
			System.out.println("");
		}

	}
}
