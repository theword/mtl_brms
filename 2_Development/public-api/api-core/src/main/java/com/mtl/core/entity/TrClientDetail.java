/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "TR_CLIENT_DETAIL", catalog = "", schema = "", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "CLIENT_ID" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "TrClientDetail.findAll", query = "SELECT t FROM TrClientDetail t"),
		@NamedQuery(name = "TrClientDetail.findByClientDetailId", query = "SELECT t FROM TrClientDetail t WHERE t.clientDetailId = :clientDetailId"),
		@NamedQuery(name = "TrClientDetail.findByClientAddress", query = "SELECT t FROM TrClientDetail t WHERE t.clientAddress = :clientAddress"),
		@NamedQuery(name = "TrClientDetail.findByClientEnabled", query = "SELECT t FROM TrClientDetail t WHERE t.clientEnabled = :clientEnabled"),
		@NamedQuery(name = "TrClientDetail.findByClientExpiredDate", query = "SELECT t FROM TrClientDetail t WHERE t.clientExpiredDate = :clientExpiredDate"),
		@NamedQuery(name = "TrClientDetail.findByClientId", query = "SELECT t FROM TrClientDetail t WHERE t.clientId = :clientId"),
		@NamedQuery(name = "TrClientDetail.findByClientSecret", query = "SELECT t FROM TrClientDetail t WHERE t.clientSecret = :clientSecret"),
		@NamedQuery(name = "TrClientDetail.findByCreatedDate", query = "SELECT t FROM TrClientDetail t WHERE t.createdDate = :createdDate"),
		@NamedQuery(name = "TrClientDetail.findByPasswordExpiredDate", query = "SELECT t FROM TrClientDetail t WHERE t.passwordExpiredDate = :passwordExpiredDate") })
public class TrClientDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "CLIENT_DETAIL_ID", nullable = false)
	private Long clientDetailId;
	@Basic(optional = false)
	@Column(name = "CLIENT_ADDRESS", nullable = false, length = 32)
	private String clientAddress;
	@Basic(optional = false)
	@Column(name = "CLIENT_ENABLED", nullable = false)
	private Boolean clientEnabled;
	@Column(name = "CLIENT_EXPIRED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date clientExpiredDate;
	@Basic(optional = false)
	@Column(name = "CLIENT_ID", nullable = false, length = 1024)
	private String clientId;
	@Basic(optional = false)
	@Column(name = "CLIENT_SECRET", nullable = false, length = 1024)
	private String clientSecret;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	@Column(name = "PASSWORD_EXPIRED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date passwordExpiredDate;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "clientDetailId", fetch = FetchType.EAGER)
	private List<TrRoleMapping> trRoleMappingList;

	public TrClientDetail() {
	}

	public TrClientDetail(Long clientDetailId) {
		this.clientDetailId = clientDetailId;
	}

	public TrClientDetail(Long clientDetailId, String clientAddress,  String clientId,
			String clientSecret, Boolean clientEnabled, Date createdDate ) {
		this.clientDetailId = clientDetailId;
		this.clientAddress = clientAddress;
		this.clientEnabled = clientEnabled;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.createdDate = createdDate;
	}

	public Long getClientDetailId() {
		return clientDetailId;
	}

	public void setClientDetailId(Long clientDetailId) {
		this.clientDetailId = clientDetailId;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public Boolean getClientEnabled() {
		return clientEnabled;
	}

	public void setClientEnabled(Boolean clientEnabled) {
		this.clientEnabled = clientEnabled;
	}

	public Date getClientExpiredDate() {
		return clientExpiredDate;
	}

	public void setClientExpiredDate(Date clientExpiredDate) {
		this.clientExpiredDate = clientExpiredDate;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getPasswordExpiredDate() {
		return passwordExpiredDate;
	}

	public void setPasswordExpiredDate(Date passwordExpiredDate) {
		this.passwordExpiredDate = passwordExpiredDate;
	}

	@XmlTransient
	public List<TrRoleMapping> getTrRoleMappingList() {
		return trRoleMappingList;
	}

	public void setTrRoleMappingList(List<TrRoleMapping> trRoleMappingList) {
		this.trRoleMappingList = trRoleMappingList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (clientDetailId != null ? clientDetailId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof TrClientDetail)) {
			return false;
		}
		TrClientDetail other = (TrClientDetail) object;
		if ((this.clientDetailId == null && other.clientDetailId != null)
				|| (this.clientDetailId != null && !this.clientDetailId.equals(other.clientDetailId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.mtl.api.entity.TrClientDetail[ clientDetailId=" + clientDetailId + " ]";
	}

}
