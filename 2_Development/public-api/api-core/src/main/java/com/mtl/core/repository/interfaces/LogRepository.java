package com.mtl.core.repository.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.mtl.core.entity.TrLogger;

public interface LogRepository extends CrudRepository<TrLogger, Long> {

}
