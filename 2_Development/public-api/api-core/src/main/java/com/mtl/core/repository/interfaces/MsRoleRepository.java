package com.mtl.core.repository.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.mtl.core.entity.MsRole;

public interface MsRoleRepository extends CrudRepository<MsRole, Long> {

}
