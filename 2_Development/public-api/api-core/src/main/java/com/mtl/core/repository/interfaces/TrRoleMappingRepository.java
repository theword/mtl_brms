package com.mtl.core.repository.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.mtl.core.entity.TrRoleMapping;

public interface TrRoleMappingRepository extends CrudRepository<TrRoleMapping, Long> {

}
