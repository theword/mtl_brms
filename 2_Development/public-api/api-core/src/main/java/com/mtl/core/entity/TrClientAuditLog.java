/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "TR_CLIENT_AUDIT_LOG", catalog = "", schema = "")
@XmlRootElement
public class TrClientAuditLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLIENT_AUDIT_LOG_ID", nullable = false)
    private Long clientAuditLogId;
    @Basic(optional = false)
    @Column(name = "ACCESS_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date accessDate;
    @Lob
    @Column(name = "ACCESS_DETAIL")
    private String accessDetail;
    @Basic(optional = false)
    @Lob
    @Column(name = "ACCESS_URL", nullable = false)
    private String accessUrl;
    @Column(name = "CLIENT_ID", length = 1024)
    private String clientId;
    @Basic(optional = false)
    @Column(name = "CLIENT_IP_ADDRESS", nullable = false, length = 32)
    private String clientIpAddress;
    @Basic(optional = false)
    @Column(name = "CLIENT_OS", nullable = false, length = 128)
    private String clientOs;
    @Column(name = "CLIENT_USER_AGENT", length = 1024)
    private String clientUserAgent;

    public TrClientAuditLog() {
    }

    public TrClientAuditLog(Long clientAuditLogId) {
        this.clientAuditLogId = clientAuditLogId;
    }

//    public TrClientAuditLog(Long clientAuditLogId, Date accessDate, String accessUrl, String clientIpAddress, String clientOs) {
//        this.clientAuditLogId = clientAuditLogId;
//        this.accessDate = accessDate;
//        this.accessUrl = accessUrl;
//        this.clientIpAddress = clientIpAddress;
//        this.clientOs = clientOs;
//    }
    

    public TrClientAuditLog(Date accessDate, String accessDetail, String accessUrl, String clientId,
			String clientIpAddress, String clientOs, String clientUserAgent) {
		super();
		this.accessDate = accessDate;
		this.accessDetail = accessDetail;
		this.accessUrl = accessUrl;
		this.clientId = clientId;
		this.clientIpAddress = clientIpAddress;
		this.clientOs = clientOs;
		this.clientUserAgent = clientUserAgent;
	}

    public Long getClientAuditLogId() {
        return clientAuditLogId;
    }

    public void setClientAuditLogId(Long clientAuditLogId) {
        this.clientAuditLogId = clientAuditLogId;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public String getAccessDetail() {
        return accessDetail;
    }

    public void setAccessDetail(String accessDetail) {
        this.accessDetail = accessDetail;
    }

    public String getAccessUrl() {
        return accessUrl;
    }

    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientIpAddress() {
        return clientIpAddress;
    }

    public void setClientIpAddress(String clientIpAddress) {
        this.clientIpAddress = clientIpAddress;
    }

    public String getClientOs() {
        return clientOs;
    }

    public void setClientOs(String clientOs) {
        this.clientOs = clientOs;
    }

    public String getClientUserAgent() {
        return clientUserAgent;
    }

    public void setClientUserAgent(String clientUserAgent) {
        this.clientUserAgent = clientUserAgent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientAuditLogId != null ? clientAuditLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrClientAuditLog)) {
            return false;
        }
        TrClientAuditLog other = (TrClientAuditLog) object;
        if ((this.clientAuditLogId == null && other.clientAuditLogId != null) || (this.clientAuditLogId != null && !this.clientAuditLogId.equals(other.clientAuditLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.api.entity.TrClientAuditLog[ clientAuditLogId=" + clientAuditLogId + " ]";
    }
    
}
