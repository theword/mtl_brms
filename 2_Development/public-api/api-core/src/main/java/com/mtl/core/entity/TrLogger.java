/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "TR_LOGGER", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrLogger.findAll", query = "SELECT t FROM TrLogger t"),
    @NamedQuery(name = "TrLogger.findById", query = "SELECT t FROM TrLogger t WHERE t.id = :id"),
    @NamedQuery(name = "TrLogger.findByEventDate", query = "SELECT t FROM TrLogger t WHERE t.eventDate = :eventDate"),
    @NamedQuery(name = "TrLogger.findByLevel", query = "SELECT t FROM TrLogger t WHERE t.level = :level"),
    @NamedQuery(name = "TrLogger.findByLogger", query = "SELECT t FROM TrLogger t WHERE t.logger = :logger")})
public class TrLogger implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "EVENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Lob
    @Column(name = "EXCEPTION")
    private String exception;
    @Column(name = "LEVEL", length = 255)
    private String level;
    @Column(name = "LOGGER", length = 255)
    private String logger;
    @Lob
    @Column(name = "MESSAGE")
    private String message;

    public TrLogger() {
    }

    public TrLogger(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLogger() {
        return logger;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrLogger)) {
            return false;
        }
        TrLogger other = (TrLogger) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.api.entity.TrLogger[ id=" + id + " ]";
    }
    
}
