package com.mtl.core.repository.interfaces;


import org.springframework.data.repository.CrudRepository;

import com.mtl.core.entity.TrClientAuditLog;

public interface ClientAuditLogRepository extends CrudRepository<TrClientAuditLog, Long> {

//	 Optional<ClientAuditLog> findOneByName(String name);
//	Page<ClientAuditLog> findAll(Pageable pageable);
}
