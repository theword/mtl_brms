package com.mtl.core.repository.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.mtl.core.entity.TrClientDetail;

import java.util.Optional;

public interface ClientDetailRepository extends CrudRepository<TrClientDetail, Long> {

	public Optional<TrClientDetail> findOptionalByClientId(String clientId);
	public Optional<TrClientDetail> findOptionalByClientIdAndClientSecret(String clientId, String clientSecret);
}
