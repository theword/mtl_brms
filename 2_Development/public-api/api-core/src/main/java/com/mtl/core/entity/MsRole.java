/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "MS_ROLE", catalog = "", schema = "", uniqueConstraints = {
	    @UniqueConstraint(columnNames = {"ROLE_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsRole.findAll", query = "SELECT m FROM MsRole m"),
    @NamedQuery(name = "MsRole.findById", query = "SELECT m FROM MsRole m WHERE m.id = :id"),
    @NamedQuery(name = "MsRole.findByRoleId", query = "SELECT m FROM MsRole m WHERE m.roleId = :roleId"),
    @NamedQuery(name = "MsRole.findByRoleDesc", query = "SELECT m FROM MsRole m WHERE m.roleDesc = :roleDesc"),
    @NamedQuery(name = "MsRole.findByIsDeleted", query = "SELECT m FROM MsRole m WHERE m.isDeleted = :isDeleted"),
    @NamedQuery(name = "MsRole.findByCreatedDate", query = "SELECT m FROM MsRole m WHERE m.createdDate = :createdDate")})
public class MsRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "ROLE_ID", nullable = false, length = 1024)
    private String roleId;
    @Column(name = "ROLE_DESC", length = 2147483647)
    private String roleDesc;
    @Column(name = "IS_DELETED")
    private Boolean isDeleted;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId", fetch = FetchType.LAZY)
    private List<TrRoleMapping> trRoleMappingList;

    public MsRole() {
    }

    public MsRole(Long id) {
        this.id = id;
    }

    public MsRole(Long id, String roleId, Date createdDate, boolean isDeleted) {
        this.id = id;
        this.roleId = roleId;
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @XmlTransient
    public List<TrRoleMapping> getTrRoleMappingList() {
        return trRoleMappingList;
    }

    public void setTrRoleMappingList(List<TrRoleMapping> trRoleMappingList) {
        this.trRoleMappingList = trRoleMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsRole)) {
            return false;
        }
        MsRole other = (MsRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.api.entity.MsRole[ id=" + id + " ]";
    }
    
}
