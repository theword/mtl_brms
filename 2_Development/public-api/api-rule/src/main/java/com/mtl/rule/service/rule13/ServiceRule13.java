package com.mtl.rule.service.rule13;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.model.collector.CollectorModel;
import com.mtl.model.request.InputModel;
import com.mtl.rule.service.impl.RuleEngineExecutor;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule13 extends RuleEngineExecutor<InputModel, CollectorModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final Logger log = LogManager.getLogger(ServiceRule13.class);

	@Override
	public boolean preAction() {
		log.info("1 preAction");
		return true;
	}

	@Override
	public void execute() {
		log.info("2 execute");
	}

	@Override
	public void finalAction() {
		log.info("3 finalAction");
	}

}
