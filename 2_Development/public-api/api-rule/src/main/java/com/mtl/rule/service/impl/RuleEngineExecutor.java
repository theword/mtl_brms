package com.mtl.rule.service.impl;

import java.io.Serializable;
import java.util.Map;

public abstract class RuleEngineExecutor<I, C> extends Thread implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String uuid;
	protected Map<String, Object> message;

	protected I input;
	protected C collecter;

	public RuleEngineExecutor() {
		super();
		uuid = this.getClass().getSimpleName().toString();

	}

	public abstract boolean preAction();

	public abstract void execute();

	public abstract void finalAction();

	@Override
	public final void run() {

		if (!preAction())
			return;

		execute();

		finalAction();
	}

	public String getUuid() {
		return uuid;
	}

	public I getInput() {
		return input;
	}

	public void setInput(I input) {
		this.input = input;
	}

	public void setCollecter(C collecter) {
		this.collecter = collecter;
	}

	public C getCollecter(C collecter) {
		return this.collecter;
	}
}
