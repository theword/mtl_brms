package com.mtl.rule.service.impl;

public abstract class AbstractSubRuleExecutor<I, C, O> {

	final public void run(I intput, C collector) {
		if (preAction(intput, collector))
			return;

		execute(intput, collector);

		finalAction();
	}

	abstract public boolean preAction(I intput, C colector);

	abstract public void execute(I intput, C colector);

	abstract public void finalAction();
}
