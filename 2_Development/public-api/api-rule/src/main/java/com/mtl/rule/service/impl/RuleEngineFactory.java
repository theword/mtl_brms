package com.mtl.rule.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.mtl.rule.service.rule13.ServiceRule13;

import lombok.Data;

@Data
@Service
public class RuleEngineFactory {

	protected static final Logger log = LogManager.getLogger(RuleEngineFactory.class);

	@Autowired
	private ApplicationContext contex;

	@PostConstruct
	public void setup() throws InterruptedException {
		Set<Class<?>> rules = new HashSet<Class<?>>();
		rules.add(ServiceRule13.class);
		getRuleEngine(rules);
	}

//	public Object myService() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
//		return Class.forName(serviceClassName).newInstance();
//	}

//	public MyInterface myService() {
//		Class<?> serviceClass = Class.forName(serviceClassName);
//		MyInterface service = MyInterface.class.cast(serviceClass.newInstance());
//		return service;
//	}

	public List<?> getRuleEngine(Set<Class<?>> rules) throws InterruptedException {

		RuleEngineExecutor<?, ?>[] threads = new RuleEngineExecutor<?, ?>[rules.size()];
		int i = 0;

		for (Class<?> rule : rules) {
			RuleEngineExecutor<?, ?> r = (RuleEngineExecutor<?, ?>) contex.getBean(rule);
			log.info(r);
			threads[i] = r;
			threads[i++].start();
		}

		for (RuleEngineExecutor<?, ?> thread : threads) {
			thread.join();
		}

		for (RuleEngineExecutor<?, ?> thread : threads) {
			log.info(thread.getUuid() + " Run success.");
		}
		
		log.info("Done all.");
		return Arrays.asList();
	}
}
