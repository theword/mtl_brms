  __  __   _______   _                 ____    _____    __  __    _____ 
 |  \/  | |__   __| | |               |  _ \  |  __ \  |  \/  |  / ____|
 | \  / |    | |    | |       ______  | |_) | | |__) | | \  / | | (___  
 | |\/| |    | |    | |      |______| |  _ <  |  _  /  | |\/| |  \___ \ 
 | |  | |    | |    | |____           | |_) | | | \ \  | |  | |  ____) |
 |_|  |_|    |_|    |______|          |____/  |_|  \_\ |_|  |_| |_____/                                                      
 :: Java JDK ::						(v@java.version@)
 :: Web Service API ::			  	(v@app.version@)
 :: Project directory ::			(@basedir@)
                