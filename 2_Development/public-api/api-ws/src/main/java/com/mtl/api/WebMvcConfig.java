package com.mtl.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	private static Logger log = LogManager.getLogger(WebMvcConfig.class);
    private final long MAX_AGE_SECS = 3600000;
    @Value("${directory.image}")
    private String directoryImage;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE")
                .maxAge(MAX_AGE_SECS);
    }
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//
//        // http
//        HttpMessageConverter converter = new StringHttpMessageConverter();
//        converters.add(converter);
//        log.info("HttpMessageConverter added");
//
//        // string
//        converter = new FormHttpMessageConverter();
//        converters.add(converter);
//        log.info("FormHttpMessageConverter added");
//
//        // json
//        converter = new MappingJackson2HttpMessageConverter();
//        converters.add(converter);
//        log.info("MappingJackson2HttpMessageConverter added");
//
//    }
    
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		System.err.println(directoryImage);
//		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//		registry.addResourceHandler("/css/theme/**").addResourceLocations("classpath:/static/css/style/");
//		registry.addResourceHandler("/gobal/**").addResourceLocations("classpath:/templates/gobal/");
//		registry.addResourceHandler("/avatar/**").addResourceLocations( env.getProperty("file.avatar.url") );
//		registry.addResourceHandler("/images/**").addResourceLocations( directoryImage, "classpath:/static/" )
		 registry.addResourceHandler("/images/**").addResourceLocations("directoryImage", "classpath:/static/" );
		 	
//		registry.addResourceHandler("/template/**").addResourceLocations( "classpath:/templates/" );
	}
 
}