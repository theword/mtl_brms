package com.mtl.api.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mtl.api.security.ClientDetail;
import com.mtl.api.security.CurrentUser;

@RestController("/${path.prefix}/sample")
public class SampleRestApi {
	private static final Logger log = LogManager.getLogger(SampleRestApi.class);

	@Secured("ROLE_USER")
	@GetMapping(consumes = { MediaType.ALL_VALUE }, produces = { MediaType.TEXT_PLAIN_VALUE })
	public @ResponseBody ResponseEntity<String> get(@CurrentUser ClientDetail currentUser) {
		log.info("Access: " + SampleRestApi.class + ", Method: " + "get()");

		return ResponseEntity.ok(currentUser.getUsername());
	}
}
