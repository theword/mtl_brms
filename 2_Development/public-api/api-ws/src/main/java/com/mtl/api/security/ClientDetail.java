package com.mtl.api.security;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.validation.constraints.Min;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;

@Validated
public class ClientDetail implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<? extends GrantedAuthority> authorities;
	private String password;
	private String username;
	private Boolean isEnabled;
	private Date accountExpiredDate;
	private Date credentialsExpiredDate;

	public ClientDetail(@Min(1) String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public ClientDetail(@Min(1) String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.username = username;
		this.password = password;
	}

	public ClientDetail( String username, String password, Collection<? extends GrantedAuthority> authorities,
			boolean isEnabled, Date accountExpiredDate, Date credentialsExpiredDate) {
		super();
		this.authorities = authorities;
		this.password = password;
		this.username = username;
		this.isEnabled = isEnabled;
		this.accountExpiredDate = accountExpiredDate;
		this.credentialsExpiredDate = credentialsExpiredDate;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		if (accountExpiredDate == null)
			return true;
		else
			return Calendar.getInstance().getTime().after(accountExpiredDate);
	}

	@Override
	public boolean isAccountNonLocked() {
		return isEnabled;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		if (credentialsExpiredDate == null)
			return true;
		else
			return Calendar.getInstance().getTime().after(credentialsExpiredDate);
	}

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @return the accountExpiredDate
	 */
	public Date getAccountExpiredDate() {
		return accountExpiredDate;
	}

	/**
	 * @param accountExpiredDate the accountExpiredDate to set
	 */
	public void setAccountExpiredDate(Date accountExpiredDate) {
		this.accountExpiredDate = accountExpiredDate;
	}

	/**
	 * @return the credentialsExpiredDate
	 */
	public Date getCredentialsExpiredDate() {
		return credentialsExpiredDate;
	}

	/**
	 * @param credentialsExpiredDate the credentialsExpiredDate to set
	 */
	public void setCredentialsExpiredDate(Date credentialsExpiredDate) {
		this.credentialsExpiredDate = credentialsExpiredDate;
	}

	/**
	 * @param authorities the authorities to set
	 */
	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
