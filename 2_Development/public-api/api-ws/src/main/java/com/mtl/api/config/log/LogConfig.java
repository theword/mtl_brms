package com.mtl.api.config.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.db.ColumnMapping;
import org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig;
import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
import org.apache.logging.log4j.core.async.AsyncLoggerConfig;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.springframework.beans.factory.annotation.Autowired;

import com.mtl.core.entity.TrLogger;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.persistence.Table;
import javax.sql.DataSource;

@org.springframework.context.annotation.Configuration
public class LogConfig {

	private static final Logger log = LogManager.getLogger(LogConfig.class); 
	public static final String DATABASE_LOGGER = "DATABASE_APPENDER";
	private static final String APPENDER_REF_NAME= "DATABASE_APPENDER";
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void onStartUp() throws SQLException {
		
		// This is the mapping between the columns in the table and what to insert in it.
		log.info("####### JDBCLog init() ########");      
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(false); 
		final Configuration config = ctx.getConfiguration();

		// Here I define the columns I want to log. 
		ColumnConfig[] columnConfigs = new ColumnConfig[] {
				ColumnConfig.newBuilder()
				.setName("EVENT_DATE")
				.setPattern(null)
				.setLiteral(null)
				.setEventTimestamp(true)
				.setUnicode(false)
				.setClob(false).build(),
				//		    ColumnConfig.newBuilder()
				//	                .setName("Source")
				//	                .setPattern("%K{className}")
				//	                .setLiteral(null)
				//	                .setEventTimestamp(false)
				//	                .setUnicode(false)
				//	                .setClob(false).build(),
				ColumnConfig.newBuilder()
				.setName("LEVEL")
				.setPattern("%level")
				.setLiteral(null)
				.setEventTimestamp(false)
				.setUnicode(false)
				.setClob(false).build(),
				ColumnConfig.newBuilder()
				.setName("MESSAGE")
				.setPattern("%message")
				.setLiteral(null)
				.setEventTimestamp(false)
				.setUnicode(false)
				.setClob(true).build(),
				ColumnConfig.newBuilder()
				.setName("EXCEPTION")
				.setPattern("%ex{full}")
				.setLiteral(null)
				.setEventTimestamp(false)
				.setUnicode(false)
				.setClob(true).build(),
				//		    ColumnConfig.newBuilder()
				//	                .setName("ProductName")
				//	                .setPattern(null)
				//	                .setLiteral("'DHC'")
				//	                .setEventTimestamp(false)
				//	                .setUnicode(false)
				//	                .setClob(false).build(),
				ColumnConfig.newBuilder()
				.setName("LOGGER")
				.setPattern("%C:%L")
				.setLiteral(null)
				.setEventTimestamp(false)
				.setUnicode(false)
				.setClob(false).build(),
				//		    ColumnConfig.newBuilder()
				//	                .setName("AuditEventType")
				//	                .setPattern("%K{eventId}")
				//	                .setLiteral(null)
				//	                .setEventTimestamp(false)
				//	                .setUnicode(false)
				//	                .setClob(false).build(),
				//		    ColumnConfig.newBuilder()
				//	                .setName("UserId")
				//	                .setPattern("%K{userId}")
				//	                .setLiteral(null)
				//	                .setEventTimestamp(false)
				//	                .setUnicode(false)
				//	                .setClob(false).build(),
				//		    ColumnConfig.newBuilder()
				//	                .setName("LogType")
				//	                .setPattern("%K{logType}")
				//	                .setLiteral(null)
				//	                .setEventTimestamp(false)
				//	                .setUnicode(false)
				//	                .setClob(false).build()
		};



		Appender jdbcAppender = JdbcAppender.newBuilder()
				.setBufferSize(30)
				.setColumnConfigs(columnConfigs)
				.setColumnMappings(new ColumnMapping[]{})
				.setConnectionSource(new JdbcConnectionSource(dataSource))
				.setTableName( TrLogger.class.getAnnotation(Table.class).name() )
				.setName(APPENDER_REF_NAME)
				.setIgnoreExceptions(false)
				.setFilter(null)
				.build();

		jdbcAppender.start();
		config.addAppender(jdbcAppender);


		// Create an Appender reference.
		// @param ref The name of the Appender.
		// @param level The Level to filter against.
		// @param filter The filter(s) to use.
		// @return The name of the Appender.
		AppenderRef ref= AppenderRef.createAppenderRef(APPENDER_REF_NAME, null, null);
		AppenderRef[] refs = new AppenderRef[] {ref};



		/*
		 * Factory method to create a LoggerConfig.
		 *
		 * @param additivity true if additive, false otherwise.
		 * @param level The Level to be associated with the Logger.
		 * @param loggerName The name of the Logger.
		 * @param includeLocation whether location should be passed downstream
		 * @param refs An array of Appender names.
		 * @param properties Properties to pass to the Logger.
		 * @param config The Configuration.
		 * @param filter A Filter.
		 * @return A new LoggerConfig.
		 * @since 2.6
		 */
		LoggerConfig loggerConfig = AsyncLoggerConfig.createLogger(
				false, Level.ERROR, DATABASE_LOGGER, Boolean.TRUE.toString(), refs, null, config, null);        
		loggerConfig.addAppender(jdbcAppender, null, null);

		config.addLogger(DATABASE_LOGGER, loggerConfig);       
		ctx.updateLoggers();  

		log.info("####### JDBCLog init() - DONE ########");  


	}



}