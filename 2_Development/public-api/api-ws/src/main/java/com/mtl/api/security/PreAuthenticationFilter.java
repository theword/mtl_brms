package com.mtl.api.security;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Calendar;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.mtl.api.service.impl.ClientAuditLogService;
import com.mtl.api.utils.ClientInfo;

@Component
public class PreAuthenticationFilter extends OncePerRequestFilter {

	private static final Logger log = LogManager.getLogger(PreAuthenticationFilter.class);
	@Value("${security.access.audit.active:false}")
	private Boolean logAuditActive;
	@Value("${h2.console.path:/h2-console}")
	private String h2ConsolePath;

	@Autowired
	private ClientAuditLogService clientAuditLogService;
	@Autowired
	private ClientDetailsService clientDetailService;
	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			if (logAuditActive && !h2ConsolePath.equalsIgnoreCase(request.getServletPath())) {
				clientAuditLogService.saveClientAuditLog(Calendar.getInstance().getTime(), null,
						ClientInfo.getFullURL(request), null, ClientInfo.getClientIpAddr(request),
						ClientInfo.getClientOS(request), null);
			}

			String credential = null;
			if ((credential = getCredentialFromRequest(request)) != null) {
				String[] values = credential.split(":");
				ClientDetail userDetails = clientDetailService.loadByClientIdAndClientSecret(values[0], values[1]);

				/**
				 * Check account isEnabled or disabled
				 */
				Optional.of(userDetails).filter(item -> Boolean.TRUE.equals(item.isEnabled()))
						.orElseThrow(() -> new DisabledException("Client id: '" + values[0] + "' is disabled."));

				/**
				 * Check account isExpired
				 */
				Optional.of(userDetails).filter(item -> !item.isAccountNonExpired())
						.orElseThrow(() -> new AccountExpiredException("Client id: '" + values[0] + "' is expired."));

				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (UsernameNotFoundException | LockedException | DisabledException | AccountExpiredException
				| CredentialsExpiredException e) {
			log.error(e);
			 SecurityContextHolder.clearContext();
			 customAuthenticationEntryPoint.commence(request, response, e);
             return;
		} 
		filterChain.doFilter(request, response);
	}

	private String getCredentialFromRequest(HttpServletRequest request) {
		String header = request.getHeader(AUTHORZE_HEADER);

		if (StringUtils.hasText(header) && ( header.toLowerCase().startsWith(AUTHORZE_TYPE))) {
			String base64Credentials = header.substring(AUTHORZE_TYPE.length(), header.length()).trim();
			byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
			return new String(credDecoded, StandardCharsets.UTF_8);
		}
		return null;
	}
	
	private static final String AUTHORZE_HEADER = "Authorization";
	private static final String AUTHORZE_TYPE = "basic";
}
