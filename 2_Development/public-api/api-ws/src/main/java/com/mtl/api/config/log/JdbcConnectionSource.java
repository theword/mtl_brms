package com.mtl.api.config.log;

import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JdbcConnectionSource implements ConnectionSource {
	private DataSource dataSource;

	
	public JdbcConnectionSource(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}
	
	@Override
	public 
	Connection getConnection() throws SQLException{
		return dataSource.getConnection();
	}
	@Override
	public String toString() {
		return JdbcConnectionSource.class.getName(); 
	 }
	@Override
	public State getState() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean isStarted() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean isStopped() {
		// TODO Auto-generated method stub
		return false;
	}
}