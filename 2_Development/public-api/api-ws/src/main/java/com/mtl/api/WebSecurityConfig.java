package com.mtl.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mtl.api.security.CustomAuthenticationEntryPoint;
import com.mtl.api.security.PasswordNoneEncoder;
import com.mtl.api.security.PreAuthenticationFilter;
import com.mtl.api.security.exception.CustomAccessDeniedHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = false)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter  {

	@Value("${security.enabled:true}")
	private boolean securityEnabled;
//	@Autowired
//	private PreAuthenticationFilter preCustomFillter;
	@Autowired
	private UserDetailsService clientDetailsService;
	@Autowired
	private CustomAuthenticationEntryPoint unauthorizedHandler;
	@Autowired
	private PasswordEncoder passwordEncode;
	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder
		.userDetailsService(clientDetailsService)
		.passwordEncoder(passwordEncode);
	}

	@Bean(org.springframework.security.config.BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	public void configure(WebSecurity webSecurity) throws Exception {
//			web.ignoring().antMatchers("/h2-console/**","/images/**", "/images/**/*");
			
		    webSecurity
            .ignoring()
//            .antMatchers(
//                HttpMethod.POST,
//                authenticationPath
//            )
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .and()
            .ignoring()
            .antMatchers(
                HttpMethod.GET,
                "/images/**" //Other Stuff You want to Ignore
            )
            .and()
            .ignoring()
            .antMatchers("/h2-console/**/**");//Should not be in Production!
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http
//			.headers().frameOptions().disable();
		
		http
		.csrf()
			.ignoringAntMatchers("/h2-console/**") //disable csrf for rest
			.ignoringAntMatchers("/images/**") //disable the database
			.ignoringAntMatchers("/images/**/*"); // allow double logout

		http
			.authorizeRequests()
				.antMatchers("/h2-console/**").permitAll()
				.antMatchers("/images/**").permitAll()
				.antMatchers("/images/**/*").permitAll()
				 .antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
				.mvcMatchers("/h2-console/**","/images/**", "/images/**/*").permitAll()
				.anyRequest().authenticated();
		http
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
				.exceptionHandling() .authenticationEntryPoint(unauthorizedHandler)
				.accessDeniedHandler(customAccessDeniedHandler);
		
		http
        .headers()
        .frameOptions().sameOrigin()  //H2 Console Needs this setting
        .cacheControl(); //disable caching
				
	}

	@Profile("!dev")
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Profile("dev")
	@Bean
	public PasswordEncoder passwordNoneEncoder() {
		return new PasswordNoneEncoder();
	}
}

