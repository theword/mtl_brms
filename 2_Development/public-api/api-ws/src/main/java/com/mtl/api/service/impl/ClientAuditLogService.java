package com.mtl.api.service.impl;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.core.repository.interfaces.ClientAuditLogRepository;
import com.mtl.core.entity.TrClientAuditLog;

@Service
public class ClientAuditLogService {

	@Autowired
	private ClientAuditLogRepository clientAuditLogRepository;

	@Transactional
	public TrClientAuditLog saveClientAuditLog(Date accessDate, String accessDetail, String accessUrl, String clientId,
			String clientIpAddress, String clientOs, String clientUserAgent) {

		TrClientAuditLog log = new TrClientAuditLog(accessDate, accessDetail, accessUrl, clientId, clientIpAddress,
				clientOs, clientUserAgent);
		return clientAuditLogRepository.save(log);

	}

}
