package com.mtl.api;

import java.util.Calendar;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.mtl.api.config.log.LogConfig;
import com.mtl.core.repository.interfaces.ClientDetailRepository;
import com.mtl.core.repository.interfaces.TrRoleMappingRepository;
import com.mtl.api.security.ClientDetail;
import com.mtl.api.security.ClientDetailsService;
import com.mtl.core.entity.MsRole;
import com.mtl.core.entity.TrClientDetail;
import com.mtl.core.entity.TrRoleMapping;

@ComponentScan("com.mtl")
@EntityScan("com.mtl.core.entity")
@EnableJpaRepositories("com.mtl.core.repository")
@EnableTransactionManagement
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class MtlApiApplication implements CommandLineRunner {

	private static final Logger log = LogManager.getContext(false).getLogger(LogConfig.DATABASE_LOGGER);
	@Autowired
	private ClientDetailRepository clientDetailRepository;
	@Autowired
	private TrRoleMappingRepository trRoleMappingRepository;
	@Autowired
	private ClientDetailsService clientDetailsService;

	public static void main(String[] args) {
		SpringApplication.run(MtlApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (clientDetailRepository.count() == 0) {
			final TrClientDetail client = clientDetailRepository.save(new TrClientDetail(null, "0.0.0.0:1", "Shopee",
					UUID.randomUUID().toString(), Boolean.FALSE, Calendar.getInstance().getTime()));

			trRoleMappingRepository.saveAll(Stream.of("ROLE_USER", "ROLE_ADMIN", "ROLE_READ", "ROLE_WRITE")
					.map(e -> new TrRoleMapping(new MsRole(null, e, Calendar.getInstance().getTime(), Boolean.FALSE),
							client, Boolean.FALSE, Calendar.getInstance().getTime()))
					.collect(Collectors.toList()));

			try {
				ClientDetail auth = (ClientDetail) clientDetailsService.loadUserByUsername("Shopee");
				log.info(auth);
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e);
			}
		}
	}

}
