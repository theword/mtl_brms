package com.mtl.api.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

public class PasswordNoneEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		return rawPassword.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		if (StringUtils.isEmpty(rawPassword) || StringUtils.isEmpty(encodedPassword)) {
			return false;
		}

		return encodedPassword.equals(rawPassword);
	}
}
