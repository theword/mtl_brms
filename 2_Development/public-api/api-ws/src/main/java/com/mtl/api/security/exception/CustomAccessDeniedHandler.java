package com.mtl.api.security.exception;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
	 
    public static final Logger LOG  = LogManager.getLogger(CustomAccessDeniedHandler.class);
 

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException)
			throws IOException, ServletException {
		String message = "";   
		Authentication auth  = SecurityContextHolder.getContext().getAuthentication();
	        if (auth != null) {
	        	message = "Client id: '" + auth.getName() 
	              + "' attempted to access the protected URL: "
	              + request.getRequestURI();
	        }else {
	        	message = "Client attempted to access the protected URL: "   + request.getRequestURI();
	        }
	        
	        LOG.warn(message);
	        response.sendError(HttpServletResponse.SC_FORBIDDEN, message);
		
	}
}