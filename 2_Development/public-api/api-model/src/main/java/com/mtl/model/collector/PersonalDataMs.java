package com.mtl.model.collector;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.model.common.IMasterModel;
import com.mtl.model.utils.DateTimeAdapter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "PersonalDataMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonalDataMs implements IMasterModel, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8959899688045866310L;
	
	@XmlElement(name = "sumPremium", required = false)
	private BigDecimal sumPremium;
	
	@XmlElement(name = "premiumSavings", required = false)
	private BigDecimal premiumSavings;
	
	@XmlElement(name = "actualPremiumPaid", required = false)
	private BigDecimal actualPremiumPaid;
	
	@XmlElement(name = "premiumPayment", required = false)
	private String premiumPayment;
	
	@XmlElement(name = "idCard", required = false)
	private String idCard;
	
	@XmlElement(name = "fullName", required = false)
	private String fullName;
	
	@XmlElement(name = "provinceCode", required = false)
	private String provinceCode;
	
	@XmlElement(name = "sex", required = false)
	private String sex;
	
	@XmlElement(name = "age", required = false)
	private BigDecimal age;

	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="birthday")
	private Date birthday;
	
	@XmlElement(name = "weight", required = false)
	private BigDecimal weight;
	
	@XmlElement(name = "birthWeight", required = false)
	private BigDecimal birthWeight;
	
	@XmlElement(name = "height", required = false)
	private BigDecimal height;
	
	@XmlElement(name = "occupationCode", required = false)
	private String occupationCode;
	
	@XmlElement(name = "agentCode", required = false)
	private String agentCode;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="requestDate")
	private Date requestDate;	
	
	@XmlElement(name = "policyNumber", required = false)
	private String policyNumber;

	@XmlElement(name = "clientNumber", required = false)
	private String clientNumber;
	
	@XmlElement(name = "nationality", required = false)
	private String nationality;
	
	private String premiumPaymentDesc;
	private int ageInMonth;
	
	@XmlElement(name = "firstName", required = false)
	private String firstName;
	
	@XmlElement(name = "lastName", required = false)
	private String lastName;
	
	@XmlElement(name = "ENFirstName", required = false)
	private String ENFirstName = "";
	
	@XmlElement(name = "ENLastName", required = false)
	private String ENLastName = "";
	
	@XmlElement(name = "ENMiddleName", required = false)
	private String ENMiddleName = "";
	
	private String agentGroup;
	private String agentGroupName;
	private String applicationNumber;
	
	@XmlElement(name = "agentName", required = false)
	private String agentName;
	@XmlElement(name = "agentLicenceNumber", required = false)
	private String agentLicenceNumber;
	@XmlElement(name = "beforeNameChange", required = false)
	private String beforeNameChange;
	@XmlElement(name = "race", required = false)
	private String race;
	@XmlElement(name = "religion", required = false)
	private String religion;
	@XmlElement(name = "maritalStatus", required = false)
	private String maritalStatus;
	@XmlElement(name = "minorMaritalStatus", required = false)
	private String minorMaritalStatus;
	@XmlElement(name = "isIDCard", required = false)
	private boolean isIDCard;
	@XmlElement(name = "houseRegistration", required = false)
	private boolean houseRegistration;
	@XmlElement(name = "other", required = false)
	private boolean other;
	@XmlElement(name = "documentDescription", required = false)
	private String documentDescription;
	@XmlElement(name = "spouseFullname", required = false)
	private String spouseFullname;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="documentExpiryDate")
	private Date documentExpiryDate;
	
	@XmlElement(name = "clientRegionAddress", required = false)
	private String clientRegionAddress = "";
	
	@XmlElement(name = "clientHouseRegistration", required = false)
	private String clientHouseRegistration = "";
	
	@XmlElement(name = "clientAllAddress", required = false)
	private String clientAllAddress = "";
	
	@XmlElement(name = "houseNumber", required = false)
	private String houseNumber;
	@XmlElement(name = "housingBuildingName", required = false)
	private String housingBuildingName;
	@XmlElement(name = "moo", required = false)
	private String moo;
	@XmlElement(name = "avenue", required = false)
	private String avenue;
	@XmlElement(name = "street", required = false)
	private String street;
	@XmlElement(name = "district", required = false)
	private String district;
	@XmlElement(name = "suburb", required = false)
	private String suburb;
	@XmlElement(name = "province", required = false)
	private String province;
	@XmlElement(name = "postalCode", required = false)
	private String postalCode;
	@XmlElement(name = "telephoneNumber", required = false)
	private String telephoneNumber;
	@XmlElement(name = "mobileNumber", required = false)
	private String mobileNumber;
	@XmlElement(name = "email", required = false)
	private String email;
	@XmlElement(name = "officeCompany", required = false)
	private String officeCompany;
	@XmlElement(name = "officeBuilding", required = false)
	private String officeBuilding ;
	@XmlElement(name = "officeNumber", required = false)
	private String officeNumber;
	@XmlElement(name = "officeMoo", required = false)
	private String officeMoo;
	@XmlElement(name = "officeAvenue", required = false)
	private String officeAvenue;
	@XmlElement(name = "officeStreet", required = false)
	private String officeStreet;
	@XmlElement(name = "officeDistrict", required = false)
	private String officeDistrict;
	@XmlElement(name = "officeSuburb", required = false)
	private String officeSuburb;
	@XmlElement(name = "officeProvince", required = false)
	private String officeProvince;
	@XmlElement(name = "officePostalCode", required = false)
	private String officePostalCode;
	@XmlElement(name = "officetelephoneNo", required = false)
	private String officetelephoneNo;
	@XmlElement(name = "presentNumber", required = false)
	private String presentNumber;
	@XmlElement(name = "presentBuilding", required = false)
	private String presentBuilding;
	@XmlElement(name = "presentMoo", required = false)
	private String presentMoo;
	@XmlElement(name = "presentAvenue", required = false)
	private String presentAvenue;
	@XmlElement(name = "presentStreet", required = false)
	private String presentStreet;
	@XmlElement(name = "presentDistrict", required = false)
	private String presentDistrict;
	@XmlElement(name = "presentSuburb", required = false)
	private String presentSuburb;
	@XmlElement(name = "presentProvince", required = false)
	private String presentProvince;
	@XmlElement(name = "presentPostalCode", required = false)
	private String presentPostalCode;
	@XmlElement(name = "presenttelephoneNo", required = false)
	private String presenttelephoneNo;
	@XmlElement(name = "presentmobileNo", required = false)
	private String presentmobileNo;
	@XmlElement(name = "presentEmail", required = false)
	private String presentEmail;
	@XmlElement(name = "mailingAddress", required = false)
	private String mailingAddress;
	@XmlElement(name = "occupationPosition", required = false)
	private String occupationPosition;
	@XmlElement(name = "occupationType", required = false)
	private String occupationType;
	@XmlElement(name = "occupationBusinessIndustry", required = false)
	private String occupationBusinessIndustry;
	@XmlElement(name = "annualIncome", required = false)
	private BigDecimal annualIncome;
	@XmlElement(name = "otherOccupation", required = false)
	private String otherOccupation;
	@XmlElement(name = "otherOccupationPosition", required = false)
	private String otherOccupationPosition;
	@XmlElement(name = "otherOccupationType", required = false)
	private String otherOccupationType;
	@XmlElement(name = "otherOccupationBusinessIndustry", required = false)
	private String otherOccupationBusinessIndustry;
	@XmlElement(name = "otherAnnualIncome", required = false)
	private BigDecimal otherAnnualIncome;
	@XmlElement(name = "motorcycleDriving", required = false)
	private boolean motorcycleDriving;
	@XmlElement(name = "basicPlanName", required = false)
	private String basicPlanName;
	@XmlElement(name = "basicPlanPar", required = false)
	private boolean basicPlanPar;
	@XmlElement(name = "dividendPayment", required = false)
	private String dividendPayment;
	@XmlElement(name = "coverageTerm", required = false)
	private BigDecimal coverageTerm;
	@XmlElement(name = "paymentTerm", required = false)
	private BigDecimal paymentTerm;
//	@XmlElement(name = "initialFundNameList", required = false)
//	private List<FundDataMs> initialFundNameList;
	@XmlElement(name = "initialTotalAllocation", required = false)
	private BigDecimal initialTotalAllocation;
//	@XmlElement(name = "topUpFundNameList", required = false)
//	private List<FundDataMs> topUpFundNameList;
	@XmlElement(name = "topUpTotalAllocation", required = false)
	private BigDecimal topUpTotalAllocation;
	@XmlElement(name = "temporaryReceiptNumber", required = false)
	private String temporaryReceiptNumber;
	@XmlElement(name = "paymentMethod", required = false)
	private String paymentMethod;
	@XmlElement(name = "bankCode", required = false)
	private String bankCode;
	@XmlElement(name = "accountNumber", required = false)
	private String accountNumber;
	@XmlElement(name = "accountName", required = false)
	private String accountName;
	@XmlElement(name = "accountBank", required = false)
	private String accountBank;
	@XmlElement(name = "accountBankBranch", required = false)
	private String accountBankBranch;
	@XmlElement(name = "paymentDescription", required = false)
	private String paymentDescription;
	@XmlElement(name = "payorFullname", required = false)
	private String payorFullname;
	@XmlElement(name = "payorAge", required = false)
	private BigDecimal payorAge;
	@XmlElement(name = "payorOccupation", required = false)
	private String payorOccupation;
	@XmlElement(name = "payorRelationship", required = false)
	private String payorRelationship;
//	@XmlElement(name = "beneficiaryDetailList", required = false)
//	private List<BeneficiaryDetailMs> beneficiaryDetailList;
	@XmlElement(name = "OtherCompanies", required = false)
	private boolean OtherCompanies;
//	@XmlElement(name = "otherCompanyDetailList", required = false)
//	private List<CompanyMs> otherCompanyDetailList;
	@XmlElement(name = "language", required = false)
	private String language;
//	@XmlElement(name = "corporateDetailList", required = false)
//	private List<CorporateDetailMs> corporateDetailList;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="businessDate")
	private Date businessDate;	
	


	// Constructor
	public PersonalDataMs(){
//		sumPremium 			= BigDecimal.ZERO;
//		premiumSavings 		= BigDecimal.ZERO;
//		actualPremiumPaid	= BigDecimal.ZERO;
//		premiumPayment 		= "";
//		premiumPaymentDesc 	= "";
//		provinceCode 		= "";
//		sex 				= "";
//		age 				= BigDecimal.ZERO;
//		ageInMonth 			= 0;
//		idCard 				= "";
//		fullName 			= "";
//		firstName 			= "";
//		lastName 			= "";
//		birthday 			= null;
//		weight 				= BigDecimal.ZERO;
////		birthWeight			= BigDecimal.ZERO;
//		height 				= BigDecimal.ZERO;
//		occupationCode 		= "";
//		agentCode 			= "";
//		agentGroup 			= "";
//		requestDate 		= null;	
//		policyNumber 		= "";
//		clientNumber 		= "";
//		agentName 			= "";
//		agentLicenceNumber 	= "";
//		applicationNumber 	= "";
//		beforeNameChange 	= "";
//		race 				= "";
//		nationality 		= "";
//		religion 			= "";
//		maritalStatus 		= "";
//		minorMaritalStatus	= "";
//		isIDCard 			= false;
//		houseRegistration 	= false;
//		other 				= false;
//		documentDescription = "";
//		spouseFullname		= "";
//		documentExpiryDate 	= null;
//		houseNumber 		= "";
//		housingBuildingName = "";
//		moo 				= "";
//		avenue 				= "";
//		street 				= "";
//		district 			= "";
//		suburb 				= "";
//		province 			= "";
//		postalCode 			= "";
//		telephoneNumber 	= "";
//		mobileNumber 		= "";
//		email 				= "";
//		officeCompany 		= "";
//		officeBuilding  	= "";
//		officeNumber 		= "";
//		officeMoo 			= "";
//		officeAvenue 		= "";
//		officeStreet 		= "";
//		officeDistrict 		= "";
//		officeSuburb 		= "";
//		officeProvince 		= "";
//		officePostalCode 	= "";
//		officetelephoneNo 	= "";
//		presentNumber 		= "";
//		presentBuilding 	= "";
//		presentMoo 			= "";
//		presentAvenue 		= "";
//		presentStreet 		= "";
//		presentDistrict 	= "";
//		presentSuburb 		= "";
//		presentProvince 	= "";
//		presentPostalCode 	= "";
//		presenttelephoneNo 	= "";
//		presentmobileNo 	= "";
//		presentEmail 		= "";
//		mailingAddress 		= "";
//		occupationPosition 	= "";
//		occupationType 		= "";
//		occupationBusinessIndustry 		= "";
//		annualIncome 		= BigDecimal.ZERO;
//		otherOccupation 	= "";
//		otherOccupationPosition 		= "";
//		otherOccupationType = "";
//		otherOccupationBusinessIndustry = "";
//		otherAnnualIncome 	= BigDecimal.ZERO;
//		motorcycleDriving 	= false;
//		basicPlanName 		= "";
//		basicPlanPar 		= false;
//		dividendPayment		= "";
//		coverageTerm 		= BigDecimal.ZERO;
//		paymentTerm 		= BigDecimal.ZERO;
//		initialFundNameList = new ArrayList<FundDataMs>();
//		initialTotalAllocation 			= BigDecimal.ZERO;
//		topUpFundNameList 	= new ArrayList<FundDataMs>();
//		topUpTotalAllocation 			= BigDecimal.ZERO;
//		temporaryReceiptNumber 			= "";
//		paymentMethod 		= "";
//		bankCode			= "";
//		accountNumber 		= "";
//		accountName 		= "";
//		accountBank 		= "";
//		accountBankBranch 	= "";
//		paymentDescription 	= "";
//		payorFullname 		= "";
//		payorAge 			= BigDecimal.ZERO;
//		payorOccupation 	= "";
//		payorRelationship 	= "";
//		beneficiaryDetailList 			= new ArrayList<BeneficiaryDetailMs>();
//		corporateDetailList           = new ArrayList<CorporateDetailMs>();
//		OtherCompanies 		= false;
//		otherCompanyDetailList			= new ArrayList<CompanyMs>();
//		language = "";
	}
	
	//common
//	public PersonalDataMs(
//			BigDecimal sumPremium, BigDecimal premiumSavings
//			, BigDecimal actualPremiumPaid, String premiumPayment, String provinceCode
//			, String sex, BigDecimal age, String idCard
//			, String firstName, String lastName, Date birthday, BigDecimal weight
//			, BigDecimal height, String occupationCode, String agentCode
//			, Date requestDate){
//		this();
//		setSumPremium(sumPremium);
//		if(premiumSavings!=null){
//			setPremiumSavings(premiumSavings);
//		}else{
//			setPremiumSavings(BigDecimal.ZERO);
//		}		
//		setActualPremiumPaid(actualPremiumPaid);
//		setPremiumPayment(premiumPayment);
//		setProvinceCode(provinceCode);
//		setSex(sex);
//		setAge(age);
//		setIdCard(idCard);
//		setFirstName(firstName);
//		setLastName(lastName);
//		setFullName(ModelUtils.getFullName(firstName, lastName));
//		setBirthday(birthday);
//		setWeight(weight);
//		setHeight(height);
//		setOccupationCode(occupationCode);
//		setAgentCode(agentCode);
//		setRequestDate(requestDate);
//		setPremiumPaymentDesc("");
//	}
	
	public void setAgeInMonth(int age){
		this.ageInMonth = age;
	}
	
	@Override
	public String getCode() {
		return idCard;
	}

	@Override
	public void setCode(String code) {
		this.idCard=code;
	}
	
	
	

}
