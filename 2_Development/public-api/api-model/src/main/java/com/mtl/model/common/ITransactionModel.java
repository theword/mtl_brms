package com.mtl.model.common;

public interface ITransactionModel{
	
	public IMasterModel getMasterModel();

	public void setMasterModel(IMasterModel masterModel);
	
}
