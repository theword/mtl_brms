package com.mtl.model.rule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;




public class RuleModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5859018784688543833L;
	protected List<RuleMessage> ruleMessages;
	protected List<RuleMessage> ruleMessagesUS;
	private boolean modelStatus;
	
	public RuleModel() {
		ruleMessages = new ArrayList<RuleMessage>();
		ruleMessagesUS = new ArrayList<RuleMessage>();
		modelStatus = true;
	}
	
	public List<RuleMessage> getRuleMesssages() {
		return ruleMessages;
	}
	
	public void setRuleMesssages(List<RuleMessage> ruleMessages) {
		this.ruleMessages = ruleMessages;
	}
	
		
	public List<RuleMessage> getRuleMesssagesUS() {
		return ruleMessagesUS;
	}

	public void setRuleMesssagesUS(List<RuleMessage> ruleMessagesUS) {
		this.ruleMessagesUS = ruleMessagesUS;
	}

	public boolean getModelStatus() {
		return modelStatus;
	}
	
	private void setModelStatus(boolean modelStatus) {
		this.modelStatus = modelStatus;
	}
	
	public void approve(){
		setModelStatus(true);
	}
	
	public void reject(){
		System.out.println("Reject the model.");
		setModelStatus(false);
	}
	
	public void addRuleMessageCode(String messageCode){
		ruleMessages.add(new RuleMessage(messageCode));
		ruleMessagesUS.add(new RuleMessage(messageCode));
	}
	
	public void addRuleMessageCodeDesc(String messageCode,String descCode){
		//System.out.println("addRuleMessageCodeDesc messageCode " +messageCode + "descCode "+descCode);
		String messageTh = "";
		String messageEn = "";
		String[] descArr = descCode.split(RuleMessage.SPLIT);
		//for Rule6x1_Message add result map uwrq US
		if(descCode.contains("\\us\\")&&descArr.length==3){		
			messageTh = descArr[0]+"!"+descArr[1];
			messageEn = descArr[0]+"!"+descArr[2].replace("\\us\\", "");
			ruleMessages.add(new RuleMessage(messageCode,messageTh));
			ruleMessagesUS.add(new RuleMessage(messageCode,messageEn));
			
		}else{
			ruleMessages.add(new RuleMessage(messageCode,descCode));
			ruleMessagesUS.add(new RuleMessage(messageCode,descCode));
		}

	}
	
	public void sortRuleMessage() {
	    if (ruleMessages ==null || ruleMessages.size()==0){
	    	return;
	    }
	    short sortIndex = (short) ruleMessages.size();
	    quicksort((short)0, (short)(sortIndex - 1));
	 }

	private void quicksort(short low, short high) {
		short i = low, j = high;
		short pivot = ruleMessages.get(low + (high-low)/2).getPriority();

		while (i <= j) {
			while (ruleMessages.get(i).getPriority() < pivot) {
				i++;
			}
			while (ruleMessages.get(i).getPriority() > pivot) {
				j--;
			}
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}
		// Recursion
		if (low < j)
			quicksort(low, j);
		if (i < high)
			quicksort(i, high);
	}

	private void exchange(short i, short j) {
		RuleMessage temp = ruleMessages.get(i);
		ruleMessages.set(i, ruleMessages.get(j));
		ruleMessages.set(j, temp);
	}
	
	public String toRuleMessageString(){
		String str = "=== rule messages ===";
		if(this.ruleMessages!=null){
			for(RuleMessage msg:this.ruleMessages){
				str = str+"\t code:"+msg.getMessageCode();
				str = str+"\t descCode:"+msg.getDescCode();
				str = str+"\t message:"+msg.getMessage();
				str = str+"\t desc:"+msg.getDesc();
				str = str+"\n";
			}	
		}
		return str;		
	}
	
	public String toRuleMessageUSString(){
		String str = "=== rule messagesUS ===";
		if(this.ruleMessagesUS!=null){
			for(RuleMessage msg:this.ruleMessagesUS){
				str = str+"\t code:"+msg.getMessageCode();
				str = str+"\t descCode:"+msg.getDescCode();
				str = str+"\t message:"+msg.getMessage();
				str = str+"\t desc:"+msg.getDesc();
				str = str+"\n";
			}	
		}
		return str;		
	}
	
	public void filterRuleMessage(){
//		boolean messageRule5Flag = false;
		
		if(ruleMessages != null && ruleMessages.size() > 0){
			for(int i = 0;i<ruleMessages.size();){
//				if(ruleMessages.get(i).getMessageCode().equals("DT05003"))
//					messageRule5Flag = true;
				if(ruleMessages.get(i).isShowMessage()){
					i++;
				}else{
					ruleMessages.remove(i);
					ruleMessagesUS.remove(i);
				}
			}//for
		}//if
		
/*
		if(messageRule5Flag){
			for(int i = 0;i<ruleMessages.size();i++){
				if(ruleMessages.get(i).getMessageCode().equals("DT04003")){
					ruleMessages.remove(i);
					break;
				}
			}//for
		}
*/
	}//filterRuleMessage
	
}
