package com.mtl.model.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class DecisionTableModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> label;
	private List<Object[]> table;

	public DecisionTableModel() {
		this.table = new ArrayList<Object[]>();
		this.label = new HashMap<Integer, String>();
	}
	
	
	public synchronized void putColumn(Integer index, String fieldName ) {
		this.label.put(index, fieldName);
	}
	

}
