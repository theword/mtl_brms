package com.mtl.model.common;

public interface IMasterModel{
	
	public String getCode();

	public void setCode(String code);
	
}
